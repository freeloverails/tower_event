Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
  resources :teams, only: [:create, :show] do
    resources :projects, only: [:index, :create, :new]
    resources :events, only: :index
  end
  resources :projects, only: [:show] do
    resources :todos, only: [:create, :show, :destroy]
  end
  get 'login' => 'sessions#new'
  delete 'logout' => 'sessions#destroy'
  post "todos/change_complete_todo" => "todos#change_complete"
  resources :todos do
    member do
      get :appoint_user, :edit_user, :edit_date, :comment
    end
  end
  resources :sessions, only: [:new, :create]
end
