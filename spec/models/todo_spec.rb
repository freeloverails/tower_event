require "rails_helper"

describe Todo do
  before :each do
    @user = FactoryGirl.create(:user)
  end
  describe "消息中心" do
    context '美恰客服推送消息' do
      it "iOS推送" do
        @mechat = FactoryGirl.build(:mechat)
        @mechat.device_os = 'iOS'
        @mechat.customized_data = {
          "RegistrationID" => "xxx",
          "用户名" => "username",
          "name" => "name",
          "设备类型" => "iPhone",
          "用户id" => "18",
          "系统版本" => "iPhone OS9.3.1",
          "sessionID" => "c0e4694a226d5529a5cbfac598ba4529",
          "avatar" => "https://app.meiqia.com/api/static/client-avatar/02-04.png"}
        @mechat.save
        puts "----Mechat推送参数----"
        puts @mechat.notification.notification.to_json
        puts "----------------------"
        expect(@mechat.notification.transmitted).to eq true
      end
    end
    context 'Post推送消息' do
      before :each do
        @post = FactoryGirl.create(:post)
      end

      it "发布后即时推送" do
        @post.article.update(published: true)
        n = @post.article.notifications.create(need_transmit: true, trigger_at: Time.now)
        job_id = n.additional_fields

        puts "----Post推送参数----"
        puts n.notification.to_json
        puts "----------------------"
        expect(job_id.is_a?(String)).to eq true
      end
    end
    # FakeWeb::NetConnectNotAllowedError:
    # Real HTTP connections are disabled. Unregistered request: PUT http://localhost:9200/products-test-new/product/1


    context '商品推送（有货 & 折扣）' do
      before :each do
        facke_product_es
        @product = FactoryGirl.create(:product)
      end

      it "发送有货商品的推送消息" do
        @user.subscribe_product('available', @product.id)
        @product.push_messages('available')
        expect(@user.notifications.select_product_notice('available').length).to eq 1
      end

      it "发送折扣商品的推送消息" do
        @user.subscribe_product('discount', @product.id)
        @product.push_messages('discount')
        expect(@user.notifications.select_product_notice('discount').length).to eq 1
      end
    end

    context 'ExtPost推送消息' do
      before :each do
        @ext_post = FactoryGirl.build(:ext_post)
        @ext_post.save(validate: false)
      end

      it "发布后推送" do
        @ext_post.update_columns(published: true)
        n = @ext_post.notifications.create(need_transmit: true, trigger_at: Time.now)
        job_id = n.additional_fields
        puts "----ExtPost推送参数----"
        puts n.notification.to_json
        puts "----------------------"
        expect(job_id.is_a?(String)).to eq true
      end
    end


    context "系统消息推送" do
      before :each do
        @sys_message = FactoryGirl.create(:sys_message)
      end

      it "need_transmit为false时不发推送" do
        expect(@sys_message.notification.need_transmit).to eq false
      end

      it "need_transmit为true时发推送" do
        @sys_message.update(need_transmit: true)
        puts "----SysMessage推送参数----"
        puts @sys_message.notification.notification.to_json
        puts "----------------------"
        expect(@sys_message.notification.need_transmit).to eq true
      end
    end
  end
end
