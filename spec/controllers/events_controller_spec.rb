require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  let(:team) { Team.create(name: "team1") }
  let(:project) { Project.create(name: "project1", team_id: team.id) }
  let(:user) { User.create(password: "12345678", nickname: "free", team_id: team.id) }
  let(:other_user) { User.create(password: "12345678", nickname: "free2", team_id: team.id) }
  let(:todo) { Todo.create(name: "todo create", desc: "todo create desc", project_id: project.id) }

  describe "event operate" do
    before do
      sign_in user
    end
    it "# index" do
      todo.create_event(current_user, Event::kinds["create_todo"], project.id)
      todo.create_event(current_user, Event::kinds["delete_todo"], todo.project.id)
      todo.create_event(current_user, Event::kinds["complete_todo"], todo.project.id)    
      todo.create_event(current_user, Event::kinds["comment"], todo.project.id)     
      get :index, params: {team_id: team.id}

      expect(response).to have_http_status(200)
    end
  end

end