require 'rails_helper'

RSpec.describe TodosController, type: :controller do
  let(:team) { Team.create(name: "team1") }
  let(:project) { Project.create(name: "project1", team_id: team.id) }
  let(:user) { User.create(password: "12345678", nickname: "free", team_id: team.id) }
  let(:other_user) { User.create(password: "12345678", nickname: "free2", team_id: team.id) }
  let(:todo) { Todo.create(name: "todo create", desc: "todo create desc", project_id: project.id) }

  describe "todo operate" do
    before do
      sign_in user
    end
    it "create todo" do
      post :create, params: {team_id: team.id, project_id: project.id, todo: {name: "todo1", end_time_at: "2017-06-16", user_id: user.id}}
      expect(response).not_to have_http_status(200)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to project_path(project)
      expect(Event.last.content).to match("创建了任务")
    end

    it "delete todo" do
      post :destroy, params: {id: todo.id}
      expect(response).to redirect_to project_path(project)
      expect(Event.last.content).to match("删除了任务")
    end

    it "change complete todo" do
      post :change_complete, params: {todo_id: todo.id}
      expect(response).not_to have_http_status(200)
      expect(Todo.last.status).to eq("finish")
      expect(Event.last.content).to match("完成了任务")
    end
  
    it "appoint user todo" do
      expect(todo.user_id).to eq(nil)
      post :appoint_user, params: {id: todo.id, appoint_user_id: other_user.id}
      expect(response).not_to have_http_status(200)
      expect(Todo.last.user_id).to eq(other_user.id)
      expect(Event.last.content).to match("给 #{other_user.nickname} 指派了任务")
    end

    it "edit user todo" do
      post :appoint_user, params: {id: todo.id, appoint_user_id: user.id}
      expect(Todo.last.user_id).to eq(user.id)
      post :edit_user, params: {id: todo.id, appoint_user_id: other_user.id}
      expect(response).not_to have_http_status(200)
      expect(Todo.last.user_id).to eq(other_user.id)
      expect(Event.last.content).to match("把 #{user.nickname} 的任务指派给 #{other_user.nickname}")
    end

    it "edit date todo" do
      expect(todo.end_time_at).to eq(nil)
      post :edit_date, params: {id: todo.id, end_time_at: "2017-06-19"}
      expect(response).not_to have_http_status(200)
      expect(Todo.last.end_time_at.strftime("%Y-%m-%d")).to eq("2017-06-19")
      expect(Event.last.content).to match("将任务完成时间从 没有截止日期 修改为 2017-06-19")

      post :edit_date, params: {id: todo.id, end_time_at: "2017-07-19"}
      expect(response).not_to have_http_status(200)
      expect(Todo.last.end_time_at.strftime("%Y-%m-%d")).to eq("2017-07-19")
      expect(Event.last.content).to match("将任务完成时间从 2017-06-19 修改为 2017-07-19")
    end

    it "comment todo" do
      post :comment, params: {id: todo.id, content: "评论内容"}
      p Comment.last
      expect(Todo.last.comments.last.content).to eq("评论内容")
      expect(Event.last.content).to match("回复了任务")
    end

  end
end