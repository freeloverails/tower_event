class AddTeamIdToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :team_id, :integer
  end
end
