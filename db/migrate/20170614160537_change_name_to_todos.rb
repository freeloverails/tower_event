class ChangeNameToTodos < ActiveRecord::Migration[5.1]
  def change
    rename_column :todos, :title, :name
  end
end
