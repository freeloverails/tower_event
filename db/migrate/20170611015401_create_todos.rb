class CreateTodos < ActiveRecord::Migration[5.1]
  def change
    create_table :todos do |t|
      t.string :title
      t.text :desc
      t.integer :user_id
      t.date :end_time_at

      t.timestamps
    end
  end
end
