# encoding=utf-8
class TodosController < ApplicationController
  before_action :signed_in_user

  def show
    @todo = Todo.find(params[:id])
  end

  def create
    project = Project.find(params[:project_id])
    todo = project.todos.build(todo_params)
    if todo.save
      todo.create_event(current_user, Event::kinds["create_todo"], project.id)
      redirect_to project_path(project)
    else
      redirect_to root_path
    end
  end

  def destroy
    @todo = Todo.find(params[:id])
    @todo.create_event(current_user, Event::kinds["delete_todo"], @todo.project.id)
    @todo.update(status: Todo.statuses["deleted"])
    redirect_to project_path(@todo.project)
  end

  def change_complete
    todo_id = params[:todo_id]
    todo = Todo.find(todo_id)
    todo.update(status: Todo.statuses["finish"])
    todo.create_event(current_user, Event::kinds["complete_todo"], todo.project.id)    
  end

  def appoint_user
    appoint_user_id = params[:appoint_user_id]
    todo = Todo.find(params[:id])
    todo.update(user_id: appoint_user_id)
    user = User.find(appoint_user_id)
    content = "给 #{user.nickname} 指派了任务"
    todo.create_event(current_user, Event::kinds["appoint_user"], todo.project.id, content)    
  end

  def edit_user
    appoint_user_id = params[:appoint_user_id]
    todo = Todo.find(params[:id])
    appoint_user = User.find(appoint_user_id)
    content = "把 #{todo.user.nickname} 的任务指派给 #{appoint_user.nickname}"
    todo.update(user_id: appoint_user_id)
    todo.create_event(current_user, Event::kinds["edit_user"], todo.project.id, content)    
  end

  def edit_date
    end_time_at = params[:end_time_at]
    todo = Todo.find(params[:id])
    before_time = todo.end_time_at
    if before_time
      before_time = before_time.strftime("%Y-%m-%d")
    else
      before_time = "没有截止日期"
    end
    content = "将任务完成时间从 #{before_time} 修改为 #{end_time_at}"
    todo.update(end_time_at: end_time_at)
    todo.create_event(current_user, Event::kinds["edit_date"], todo.project.id, content)    
  end

  def comment
    todo = Todo.find(params[:id])
    comment = todo.comments.create(user_id: current_user.id, content: params[:content])
    todo.create_event(current_user, Event::kinds["comment"], todo.project.id)    
  end

  private
    def todo_params
      params.require(:todo).permit(:name, :end_time_at, :user_id)
    end

end