# encoding=utf-8
class HomeController < ApplicationController
  before_action :signed_in_user

  def index
    if team = current_user.team
      redirect_to team_projects_path(team)
    else
      @team = Team.new
    end
  end

end