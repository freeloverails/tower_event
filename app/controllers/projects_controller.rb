# encoding=utf-8
class ProjectsController < ApplicationController
  before_action :signed_in_user
  before_action :access_user, only: :show

  def index
    @team = Team.find(params[:team_id])
    @projects = @team.projects    
  end

  def new
    @team = Team.find(params[:team_id])
    @project = @team.projects.build
    @members = @team.users
  end

  def create
    user_ids = params[:user_ids]
    @team = Team.find(params[:team_id])
    project = @team.projects.build(project_params)
    if project.save
      user_ids.each do |user_id|
        Access.create(user_id: user_id, project_id: project.id)
      end
      redirect_to project_path(project)
    else
      redirect_to root_path
    end
  end

  def show
    @todos = @project.todos
    @unfinish_count = @todos.unfinish.count
    @project_users = @project.users
    @todos = @todos.order(id: :desc).paginate(:page => params[:page])
  end

  private
    def project_params
      params.require(:project).permit(:name)
    end

    def access_user
      @project = Project.find(params[:id])
      unless @project.is_access?(current_user.id)
        flash[:success] = "没有权限访问该项目"
        redirect_to root_path 
      end
    end

end