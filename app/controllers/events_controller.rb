# encoding=utf-8
class EventsController < ApplicationController
  before_action :signed_in_user

  def index
    @team = Team.find(params[:team_id])
    @projects = @team.projects
    @members = @team.users
    @events = @team.events
    @events = @events.select_user(params[:user_id]) if params[:user_id].present?
    @events = @events.select_eventable(params[:project_id], "Project") if params[:project_id].present?
    @events = @events.order(id: :desc).paginate(:page => params[:page])
    respond_to do |format|
      format.html { render 'index' }
      format.json  { render json: @events }
    end
  end

end