# encoding=utf-8
class TeamsController < ApplicationController

  def create
    team = Team.new(team_params)
    if team.save
      current_user.update(team_id: team.id)
      redirect_to team_projects_path(team)
    else
      redirect_to root_path
    end
  end

  private
    def team_params
      params.require(:team).permit(:name)
    end

end