jQuery(function() {
  if ($('.endless_paginate').length) {
    $(window).scroll(function() {
      var url = $('.endless_paginate .next_page').attr('href');
      if (url && $(window).scrollTop() > $(document).height() - $(window).height() - 100) {
        $('.endless_paginate').text("努力加载中...");
        console.log(url);
        return $.getScript(url);
      }
    });
    return $(window).scroll();
  }
});