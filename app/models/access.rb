class Access < ApplicationRecord
  # enum role: { admin: 0, member: 1 }
  belongs_to :project
  belongs_to :user
end
