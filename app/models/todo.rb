class Todo < ApplicationRecord
  include Eventable
  belongs_to :user, required: false
  belongs_to :project
  has_many :comments, dependent: :destroy
  enum status: { init: 0, progress: 1, suspend: 2, finish: 3, deleted: 4 }
  scope :unfinish, -> { where(status: [0, 1, 2]) }

  def is_complete?
    self.status == "finish" ? true : false
  end
end
