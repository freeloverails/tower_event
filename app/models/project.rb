class Project < ApplicationRecord
  include Eventable
  has_many :todos, dependent: :destroy
  has_many :accesses, dependent: :destroy
  has_many :users, through: :accesses

  def is_access?(user_id)
    self.accesses.where(user_id: user_id).empty? ? false :true
  end
end
