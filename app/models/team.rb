class Team < ApplicationRecord
  has_many :projects, dependent: :destroy
  has_many :users, dependent: :destroy
  has_many :events, dependent: :destroy
end
