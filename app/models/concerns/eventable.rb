module Eventable
  extend ActiveSupport::Concern

  included do
    # eventable: todo, project
    has_many :events, as: :eventable, dependent: :destroy

    def create_event(user, kind, project_id, content="")
      case kind
      when Event::kinds["create_todo"]
        content = "创建了任务"
      when Event::kinds["delete_todo"]
        content = "删除了任务"
      when Event::kinds["complete_todo"]
        content = "完成了任务"
      when Event::kinds["appoint_user"]
        content = content
      when Event::kinds["edit_user"]
        content = content
      when Event::kinds["edit_date"]
        content = content
      when Event::kinds["comment"]
        content = "回复了任务"
      end
      self.events.create(user_id: user.id, content: content, kind: kind, team_id: user.team_id, project_id: project_id)
    end
  end
end