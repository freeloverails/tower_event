class Event < ApplicationRecord
  self.per_page = 50
  belongs_to :eventable, polymorphic: true, dependent: :destroy
  belongs_to :team
  belongs_to :user
  belongs_to :project
  enum kind: {
    create_todo: 0, delete_todo: 1, 
    complete_todo: 2, appoint_user: 3, 
    edit_user: 4, edit_date: 5, 
    comment: 6, create_object: 7
  }
  CN_WEEK = {
    "Monday" => "周一",
    "Tuesday" => "周二",
    "Wednesday" => "周三",
    "Thursday" => "周四",
    "Friday" => "周五",
    "Saturday" => "周六",
    "Weekday" => "周日"
  }
  # KIND = {
  #   0 => "创建了任务",
  #   1 => "删除了任务",
  #   2 => "完成了任务",
  #   3 => "给 free 指派了任务",
  #   4 => "把 free 的任务指派给 Victor",
  #   5 => "将任务完成时间从 没有截止日期 修改为 下周五 ",
  #   6 => "回复了任务"
  # 开始处理这条任务
  # 暂停处理这条任务
  # 创建了项目
  # 取消了 free 的任务
  # 恢复了任务
  # 重新打开了任务
  # }
  scope :select_user, ->(user_id) { where(user_id: user_id) }
  scope :select_eventable, ->(eventable_id, eventable_type) { where(eventable_id: eventable_id, eventable_type:eventable_type) }
end
